def get_tatoeba_list():
    sentence_list = []
    with open('xmls/tatoeba.utf', 'rb') as file:
        lines = [f.decode() for f in file.readlines()]
        sentence_pair = []
        for counter, line in enumerate(lines):
            if counter % 2 == 0:
                sentence_ja, sentence_eng = line.split('\t')
                sentence_eng = sentence_eng.split('#')[0]
                sentence_furigana = lines[counter + 1]
                sentence_list.append((sentence_ja[3:], sentence_eng, sentence_furigana[3:]))

