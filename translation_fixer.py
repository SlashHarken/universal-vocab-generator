# coding=utf-8
the_list = ["Noun", "common noun", "general", "adverbable", "participant", "case particle", "participant particle",
            "verb", "non-independent", "upper line-a line", "Natural-general", "Auxiliary verb", "Auxiliary verb-Nai",
            "Terminal-general", "Five-layer-line", "Consecutive-sounding stool", "Connecting particle",
            "Stable changeable", " Auxiliary Verb-D ", " Continuous Form-General ", " Sa Line Variation ",
            " Natural Form-S ", " Lower Level-Sa Line ", " Continuous Form-General ", " Adjunct ", " Pronoun ",
            " Suffix " "Lite", "Noun", "Adverb", "Lower Level 1 -A Line", "Combined Participants", "Adjectives",
            "Five Levels -Wah Line", "Intentional Form", "Final Particle", "Adjective", "Possible adjective",
            "dual form - ni ", " quasi - participant particle ", " auxiliary verb stem ", " auxiliary verb - ta ",
            " naked form - se ", " auxiliary verb - nu ", " final form - repellent stool ", "Auxiliary Verb - Thai",
            "Auxiliary Verb - Death", "Proper Noun", "Personal Name", "Conjunction", "Five - Ta Line",
            "Auxiliary Verb - Lel", "Lower Level - La Line", "Five" "Stage - line", "Upper one - line",
            "Name", "Instruction", "Fifth - line", "Consecutive - acoustic", "Assumption - general",
            "Classifier", "Stem - General ", "Number", "Under one stage - mosquito line", "prefix",
            "five - stage - Ma line"]
original_list = ["名詞", "普通名詞", "一般", "副詞可能", "助詞", "格助詞", "係助詞", "動詞", "非自立可能", "上一段-ア行", "未然形-一般", "助動詞", "助動詞-ナイ", "終止形-一般", "五段-ラ行", "連用形-促音便", "接続助詞", "サ変可能", "助動詞-ダ", "連用形-一般", "サ行変格", "未然形-サ", "下一段-サ行", "連体形-一般", "副助詞", "代名詞", "接尾辞", "名詞的", "副詞", "下一段-ア行", "連体詞", "形容詞", "五段-ワア行", "意志推量形", "終助詞", "形状詞", "形状詞可能", "連用形-ニ", "準体助詞", "助動詞語幹", "助動詞-タ", "未然形-セ", "助動詞-ヌ", "終止形-撥音便", "助動詞-タイ", "助動詞-デス", "固有名詞", "人名", "接続詞", "五段-タ行", "助動詞-レル", "下一段-ラ行", "五段-サ行", "上一段-カ行", "名", "命令形", "五段-ナ行", "連用形-撥音便", "仮定形-一般", "助数詞", "語幹-一般", "数詞", "下一段-カ行", "接頭辞", "五段-マ行"]
fixed_eng_list = []
the_dict = {}
for element in the_list:
    fixed_eng_list.append(element.strip())
for english, ja in zip(fixed_eng_list):
    the_dict[ja] = english
print(the_dict)
