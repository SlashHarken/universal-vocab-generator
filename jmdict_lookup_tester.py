from sudachi_functions import word_lookup, which_script_is_it, normalize_poses, get_pos_translation, kata_to_hira
from variables import tokenizer_obj, morph_list_ja, list_of_pos

morph_list = tokenizer_obj.tokenize('だが、何をだろうか。私は、一体、何を忘れているのだろうか? '
                                    'しかし、疑問に向き合う間もなくソレは突如として震え出す。')
for morph in morph_list:  # This loop throws out anything not japanese
    norm_form = morph.normalized_form()
    if which_script_is_it(norm_form) is not False:
        ja_parts_of_speech = [f for f in morph.part_of_speech() if '*' not in f]  # Filter out empty fields
        raw_parts_of_speech = get_pos_translation(ja_parts_of_speech)
        if any('particle' in f.lower() for f in raw_parts_of_speech): continue  # Ignore particles
        parts_of_speech = normalize_poses(raw_parts_of_speech, norm_form)
        parts_of_speech = [f.lower() for f in parts_of_speech]
        morph_dict = {'morpheme': morph, 'ktype': which_script_is_it(norm_form),
                      'pos': parts_of_speech, 'raw_pos': raw_parts_of_speech, 'ja_pos': ja_parts_of_speech}
        if morph_dict not in morph_list_ja:
            morph_list_ja.append(morph_dict)
        for pos in morph_list_ja[-1]['pos']:  # This creates a list of all parts of speech found in all words
            if pos not in list_of_pos:  # With no duplicates
                list_of_pos.append(pos)
    else:
        pass  # Ignores words that had no japanese characters
del morph_list  # Get rid of the old, unprocessed list

for morph_dict in morph_list_ja:
    morph = morph_dict['morpheme']
    parts_of_speech, raw_parts_of_speech, ja_pos = morph_dict['pos'], morph_dict['raw_pos'], morph_dict['ja_pos']
    print(morph.normalized_form(), morph.dictionary_form(), kata_to_hira(morph.reading_form()), parts_of_speech,
          raw_parts_of_speech, ja_pos)
