#!/usr/bin/env python
# -*- coding: utf-8 -*
from genanki import Note, Package

from anki_definitions import univocabmodel, universal_vocab_deck
from sudachi_functions import which_script_is_it, get_pos_translation, normalize_poses, word_lookup, kata_to_hira, \
    WordAnswer
from variables import tokenizer_obj, morph_list_ja

if __name__ == '__main__':
    with open('txttoscan/texttotest.txt', 'rb') as file:
        txt = file.read().decode()
    # list_of_sentences = [f.replace('\n', '') + '。' for f in txt.split('。')]
    # pool = Pool(6)
    final_word_dict = {}
    # for sentence in list_of_sentences:
    #     for morph in tokenizer_obj.tokenize(sentence, mode):
    #         morph_list.append([morph, sentence])
    # with open('txttoscan/crossingfield.txt', 'rb') as file:
    #     txt = file.read().decode()
    morph_list = tokenizer_obj.tokenize(txt)
    # All morphemes sensible or not
    for morph in morph_list:  # This loop throws out anything not japanese
        norm_form = morph.normalized_form()
        if which_script_is_it(norm_form) is not False:
            ja_parts_of_speech = [f for f in morph.part_of_speech() if '*' not in f]  # Filter out empty fields
            raw_parts_of_speech = get_pos_translation(ja_parts_of_speech, norm_form)
            parts_of_speech = normalize_poses(raw_parts_of_speech, norm_form)
            parts_of_speech = [f.lower() for f in parts_of_speech]
            morph_dict = {'morpheme': morph, 'ktype': which_script_is_it(norm_form),
                          'pos': parts_of_speech, 'raw_pos': raw_parts_of_speech, 'ja_pos': ja_parts_of_speech}
            if morph_dict not in morph_list_ja:
                morph_list_ja.append(morph_dict)
            for pos in morph_list_ja[-1]['pos']:  # This creates a list of all parts of speech found in all words
                if pos not in list_of_pos:  # With no duplicates
                    list_of_pos.append(pos)
        else:
            pass  # Ignores words that had no japanese characters
    del morph_list  # Get rid of the old, unprocessed list
    # myougiden_handler('人間性', ['Noun'])
    # exit()
    command_list = []
    for morph in morph_list_ja:
        mph = morph['morpheme']
        pos = morph['pos']
        raw_pos = morph['raw_pos']
        ja_pos = morph['ja_pos']
        word_type = morph['ktype']
        # if 'hiragana' in word_type:  #  Helped for myougiden but just messed with the db method
        #     pos = pos + ['uk']
        norm_form = mph.normalized_form()
        dictionary_form = mph.dictionary_form()
        reading_form = mph.reading_form()
        # print(norm_form, dictionary_form, reading_form)
        sudachi_reading = kata_to_hira(reading_form)
        morph_id = norm_form + str(pos)
        if morph_id not in final_word_dict:  # Keeps the same word from being added again
            # response_myougi = f'{norm_form} - {myougiden_handler(mph.normalized_form(), pos)}'
            final_word_dict[morph_id] = 'Placeholder'
            # print(f'{norm_form} - {myougiden_handler(mph.normalized_form(), pos)}')
            # print('----------------')
            command_list.append((norm_form, word_type, pos, sudachi_reading))
        else:
            pass
    for command in command_list:
        answer = word_lookup(command)
        if answer is None:
            print(f'No good answer for {command}')
        else:
            answer_classes.append(WordAnswer(answer))
            # print(answer_classes[-1].nice_formatting())
    for element in answer_classes:
        my_note = Note(model=univocabmodel,
                       fields=[element.word, element.reading, element.definitions, element.poses, element.pri_tags],
                       tags=['Universal Vocab'])
        universal_vocab_deck.add_note(my_note)
    my_package = Package(universal_vocab_deck)
    my_package.write_to_file('vocabdeck.apkg')
    print('Reached the end.')
    # results = pool.starmap(myougiden_handler, command_list)
    # print('\n'.join([str(f) for f in results]))
    # print(norm_form + '#' + '|'.join(pos) + '#' + '|'.join(raw_pos))
    # print(morph['morpheme'].word_id())
    # print(morph['morpheme'].dictionary_id())
    # norm_form = morph['morpheme'].normalized_form()
    # word_type = morph['ktype']
    # print(f'{norm_form} - {word_type}')
    # print(morph['pos'])
    # print(word_lookup(norm_form, word_type))
