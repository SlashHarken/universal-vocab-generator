#!/usr/bin/env python
# -*- coding: utf-8 -*
import logging

from regex import compile as x_compile
from sqlalchemy import MetaData, Table, Column, Text
from sqlalchemy.engine import create_engine
from sudachipy import dictionary, tokenizer

path_to_myougiden = '\\Scripts\\myougiden'  # Myougiden isn't used currently
myougi_pos_responses = {}  # Cache responses to the -a query to myougiden
tokenizer_obj = dictionary.Dictionary().create()
mode = tokenizer.Tokenizer.SplitMode.C
translate_myougi_codes = ['python', path_to_myougiden, '-a']  # Add a myougiden part of speech to get a definition
all_unrecognized_pos = []  # Holds parts of speech that weren't recognized by normalize_poses
pos_dict = {'名詞': 'Noun', '普通名詞': 'common noun', '一般': 'general', '副詞可能': 'adverbable', '助詞': 'participant',
            '格助詞': 'case particle', '係助詞': 'participant particle', '動詞': 'verb', '非自立可能': 'non-independent',
            '上一段-ア行': 'upper line-a line', '未然形-一般': 'Natural-general', '助動詞': 'Auxiliary verb',
            '助動詞-ナイ': 'Auxiliary verb-Nai', '終止形-一般': 'Terminal-general', '五段-ラ行': 'Five-layer-line',
            '連用形-促音便': 'Consecutive-sounding stool', '接続助詞': 'Connecting particle', 'サ変可能': 'Stable changeable',
            '助動詞-ダ': 'Auxiliary Verb-D', '連用形-一般': 'Continuous Form-General', 'サ行変格': 'Sa Line Variation',
            '未然形-サ': 'Natural Form-S', '下一段-サ行': 'Lower Level-Sa Line', '連体形-一般': 'Continuous Form-General',
            '副助詞': 'Adjunct', '代名詞': 'Pronoun', '接尾辞': 'Suffix Lite', '名詞的': 'Noun', '副詞': 'Adverb',
            '下一段-ア行': 'Lower Level 1 -A Line', '連体詞': 'Combined Participants', '形容詞': 'Adjectives',
            '五段-ワア行': 'Five Levels -Wah Line', '意志推量形': 'Intentional Form', '終助詞': 'Final Particle', '形状詞': 'Adjective',
            '形状詞可能': 'Possible adjective', '連用形-ニ': 'dual form - ni', '準体助詞': 'quasi - participant particle',
            '助動詞語幹': 'auxiliary verb stem', '助動詞-タ': 'auxiliary verb - ta', '未然形-セ': 'naked form - se',
            '助動詞-ヌ': 'auxiliary verb - nu', '終止形-撥音便': 'final form - repellent stool',
            '助動詞-タイ': 'Auxiliary Verb - Thai', '助動詞-デス': 'Auxiliary Verb - Death', '固有名詞': 'Proper Noun',
            '人名': 'Personal Name', '接続詞': 'Conjunction', '五段-タ行': 'Five - Ta Line', '助動詞-レル': 'Auxiliary Verb - Lel',
            '下一段-ラ行': 'Lower Level - La Line', '五段-サ行': 'FiveStage - line', '上一段-カ行': 'Upper one - line', '名': 'Name',
            '命令形': 'Instruction', '五段-ナ行': 'Fifth - line', '連用形-撥音便': 'Consecutive - acoustic',
            '仮定形-一般': 'Assumption - general', '助数詞': 'Classifier', '語幹-一般': 'Stem - General', '数詞': 'Number',
            '下一段-カ行': 'Under one stage - mosquito line', '接頭辞': 'prefix',
            '五段-マ行': 'five - stage - Ma line', '助数詞可能': 'Classifier possible', '下一段-ナ行': 'under one stage na-line',
            '下一段-ガ行': 'verb', '感動詞': 'impression verb',
            '形容詞的': 'adjective', '五段-カ行': 'verb', '上一段-タ行': 'verb', '連用形-イ音便': 'verb'}
# ^ Maps the jp pos codes from sudachi to their english equivalent (According to google translate)
jmdict_engine = create_engine('sqlite:///databases/jmdict.sqlite', echo=False)  # Database stuff
jmdict_meta = MetaData()
jmdict_cmds = []
jmdict_conn = jmdict_engine.connect()
jmdict_table = Table('jmdict_table', jmdict_meta,
                     Column('ent_seq', Text, primary_key=True),
                     Column('raw_info', Text), )
jmdict_meta.create_all(jmdict_engine)  # Below all the regex related stuff
ja_x_compile = x_compile(
    r'((\p{IsHan})|\p{IsHiragana}|\p{IsKatakana})')  # Returns True if there are any kind of ja text
has_kanji_x_compile = x_compile(r'\p{IsHan}')  # Returns True if it finds kanji
has_katakana_x_compile = x_compile(r'(\p{IsKatakana})')  # Returns True if it finds katakana
morph_list_ja = []  # A final list with non-japanese morphemes thrown out
list_of_pos, answer_classes = [], []
logging.basicConfig(filename='app.log', format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %I:%M:%S %p',
                    level=logging.INFO)