from gzip import open as gz_open
from os import mkdir
from os.path import isfile, isdir

from bs4 import BeautifulSoup
from wget import download as wget_dl, bar_adaptive as wget_bar

from variables import jmdict_conn, jmdict_table, jmdict_cmds

path_to_xml = './databases/JMdict_e'
path_to_gzip = './JMdict_e.gz'
path_to_databases = './databases'

if not isdir(path_to_databases):
    mkdir(path_to_databases)

if not isfile(path_to_gzip):
    link = 'ftp://ftp.monash.edu.au/pub/nihongo/JMdict_e.gz'
    filename = wget_dl(link, bar=wget_bar)
if not isfile(path_to_xml):
    with gz_open(path_to_gzip) as gzip_file:
        file_data = gzip_file.read()
    with open(path_to_xml, 'wb') as file:
        file.write(file_data)


def jmdict_sql_gen(demo=False):
    index = 0
    if demo is False:
        dict_xml_path = path_to_xml
    else:
        print('Doing a simple demo. Small subset of words being used.')
        dict_xml_path = 'xmls/sample_jmdict.xml'
    with open(dict_xml_path, 'rb') as file:
        all_soup = BeautifulSoup(file.read().decode(), 'lxml')
    for entry in all_soup.find_all('entry'):
        # num = entry.find('ent_seq').text
        jmdict_cmds.append({'ent_seq': index, 'raw_info': str(entry)})
        index += 1
    jmdict_conn.execute(jmdict_table.insert(), jmdict_cmds)


if __name__ == '__main__':
    jmdict_sql_gen()