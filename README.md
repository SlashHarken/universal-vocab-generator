Currently not really meant for end-users.

## Main Features
* Download JMdict with database_gen.py and convert it into a sqlite database for fast searches
* Use SudachiPy to scan text and generate a list of words with part-of-speech tags
* Attempt to use that information to find the correct definition in JMdict
* Generate Anki Deck with decent styling/formatting

## Third-Party Libraries/Files
* JMdict
* SudachiPy
* genanki
* sqlalchemy
* romkan
* BeautifulSoup
* wget

## To-Do
* Further improve definition retrieval accuracy
* Create a separate anki deck with all the kanji the words use or all that don't appear in various kanji learning programs (RTK, Kodansha whatever, etc.) using KanjiDict to filter out the ones that are already known
* Automatically pull example sentences for the words from Tanaka Corpus/Tatoeba