from genanki import Model, Deck

#  Temporary CSS from a different deck
style_css = "@font-face { font-family: stroke; src: url('_stroke.ttf'); }@font-face { font-family: textbook; src: " \
            "url('_HGSKyokashotai.ttf'); }@font-face {   font-family: 'notosans';   font-style: normal;   " \
            "font-weight: 400;   src: url(http://fonts.gstatic.com/ea/notosansjapanese/v6/NotoSansJP-Regular.otf) " \
            "format('opentype'); }@font-face {   font-family: 'localnoto';   font-style: normal;   font-weight: 400;   " \
            "src: url('_NotoSansJP-Medium.otf') format('opentype'); }.card { font-family:  'localnoto', 'notosans', " \
            "'ヒラギノ明朝 ProN', 'Hiragino Mincho Pro', 'serif'; font-size: 25px; text-align: center;}img { " \
            "max-height: 150px; margin: 10px;}div { padding-top:5px; padding-bottom:5px;}a { color: turquoise; margin: " \
            "5px;}.ios-only { display: none; }.mac-only { display: none; }.mobile .ios-only { display: inline; }.mac " \
            ".mac-only { display: inline; }.stroke { font-family: 'stroke'; }.japanese { font-family: 'textbook'; }.mac" \
            "{ font-family: 'ヒラギノ明朝 ProN', 'Hiragino Mincho Pro', 'serif'; }.mac .stroke { font-family: stroke; " \
            "}.mac .japanese { font-family: textbook; }.mobile { font-family: textbook, 'Hiragino Mincho Pro', " \
            "'serif'; }"

univocabmodel = Model(14546564, 'Vocab Reading Card',
                      fields=[{'name': 'Vocab-Kanji'}, {'name': 'Vocab-Furigana'}, {'name': 'Vocab-English'},
                              {'name': 'Parts Of Speech'}, {'name': 'Frequency'}],
                      templates=[{'name': 'Reading',
                                  'qfmt': '<div class="japanese" style="font-size:60px;">{{Vocab-Kanji}}</div><div '
                                          'style="font-size: 16px; ">{{Parts Of Speech}}</div><br/><br/><br/><br/><br/>',
                                  'afmt': '<div class="japanese" '
                                          'style="font-size:60px;">{{Vocab-Kanji}}<br/>[{{Vocab-Furigana}}]</div><div '
                                          'style="font-size: 14px; ">{{Parts Of Speech}}</div><hr id=answer><div '
                                          'style="font-size: 30px; ">{{Vocab-English}}</div><div '
                                          'style="font-size:10pt;">{{Frequency}}</div>'}, ],
                      css=style_css)

universal_vocab_deck = Deck(456865235, 'Universal Vocab')
