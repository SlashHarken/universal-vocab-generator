import re
from os import mkdir
from os.path import isdir
from subprocess import Popen, PIPE

from bs4 import BeautifulSoup
from romkan import to_roma, to_hiragana
from translate_jmdict_pos_codes import trans_jm_code
from variables import ja_x_compile, has_kanji_x_compile, has_katakana_x_compile, pos_dict, all_unrecognized_pos, \
    path_to_myougiden, myougi_pos_responses, translate_myougi_codes, jmdict_conn


if not isdir('./databases'):  # Holds the extracted JMdict file and the sqlite database file
    mkdir('./databases')


def which_script_is_it(word):  # Tells if something should be marked as kanji, katakana, or hiragana
    if ja_x_compile.match(word) is not None:
        if has_kanji_x_compile.search(word):
            return 'kanji'
        else:
            if has_katakana_x_compile.search(word) is not None:
                return 'katakana'
            else:
                return 'hiragana'
    else:  # If it's just some random garbage
        return False


def loggy(message):  # The extremely simple solution to dual logging/printing to terminal :p
    # print(message)
    with open('app.log', 'wb') as log_file:
        log_file.write(message + '\n'.encode('utf-8'))


def get_pos_translation(raw_pos_list):
    temp_list = []
    for ja_pos in raw_pos_list:
        try:
            translation_result = pos_dict[ja_pos]
            temp_list.append(translation_result)
        except KeyError:
            temp_list.append(ja_pos)
            # print(f'{norm_form} with {ja_pos} had no matches.')
    if len(temp_list) > 0:
        return temp_list
    else:
        return 'No matches found in ' + str(raw_pos_list)


def pbm_scorer(pos_list, pos_myougi_definitions, pri_popularity):
    # Takes a morpheme's and word_lookup response's parts of speech lists and compares them to generate a similarity
    # score.
    score = 0
    assert type(pos_list) is list and type(pos_myougi_definitions) is list
    try:
        assert len(pos_list) > 0 and len(pos_myougi_definitions) > 0
    except AssertionError:
        pass
    for f in pos_list:
        try:
            if any(f in i for i in pos_myougi_definitions):
                score += 1
        except TypeError:
            return 0
    if score != 0:
        score = score + pri_popularity
    # pos_list = [f.lower() for f in pos_list]
    # cleaned_pos_list = []
    # for f in pos_list:
    #     if 'adjective' in f:
    #         cleaned_pos_list.append('adj')
    #     if 'possible adjective' in f:
    #         cleaned_pos_list.append('adjectival')
    #     if 'auxiliary verb' in f:
    #         cleaned_pos_list.append('verbal')
    #         cleaned_pos_list.append('auxiliary verb')
    #     else:
    #         cleaned_pos_list.append(f)
    # for f in cleaned_pos_list:
    #     if any(f in i for i in pos_myougi_definitions):
    #         score += 1
    return score


def normalize_poses(parts_of_speech_list, gmp_norm_form):  # Tries to guess the main part of speech of a word (noun,
    # adverb, bleh)
    type_list = []  # Will eventually be the final normalized list
    try:  # If, for example, sudachipy/google-translate gave me a pos like 'particle-keiyodishi-lbajkd-ⒺяţⒹ𝔤𝔫ᕼғ¢ţ千'
        # the following code would make that just 'particle' so I can actually use it to find the right dictionary
        # definition
        if any('particle' in f.lower() for f in parts_of_speech_list):
            type_list.append('Particle')
        if any('noun'.lower() in f.lower() for f in parts_of_speech_list):
            type_list.append('Noun')
        if any('adverb' in f.lower() for f in parts_of_speech_list):
            type_list.append('Adverb')
        if any('verb' in f.lower() for f in parts_of_speech_list):
            if any('auxiliary verb' in f.lower() for f in parts_of_speech_list):
                type_list.append('Auxiliary Verb')
            else:
                type_list.append('Verb')
        if any('adjective' in f.lower() for f in parts_of_speech_list):
            if any('possible adjective' in f.lower() for f in parts_of_speech_list):
                type_list.append('Adjective')
                type_list.append('Possible Adjective')
            else:
                type_list.append('Adjective')
        if any('prefix' in f.lower() for f in parts_of_speech_list):
            type_list.append('Prefix')
        if any('suffix' in f.lower() for f in parts_of_speech_list):
            type_list.append('Suffix')
        if any('conjunction' in f.lower() for f in parts_of_speech_list):
            type_list.append('Conjunction')
        if len(type_list) == 0:
            all_unrecognized_pos.append({'word': gmp_norm_form, 'parts of speech': parts_of_speech_list})
        return type_list
    except TypeError:
        print(gmp_norm_form + ' ' + parts_of_speech_list)


def kata_to_hira(katakana):  # The name should make it self-explanatory
    roma = to_roma(katakana)
    hira = to_hiragana(roma)
    return hira


def myougiden_handler(word, list_of_eng_sudachi_pos):
    pos_response_myougi = 'PLACEHOLDER'
    highest_score = 0
    final_definition = 'Not Found'
    response = Popen(['python', path_to_myougiden, '-t', word], stdout=PIPE).communicate()[0].decode()
    # print(response)
    split_response = [f for f in response.split('	') if ja_x_compile.match(f)
                      is None]  # A list of candidates for the best answer
    try:
        kana_reading = [f for f in response.split('	') if ja_x_compile.match(f) is not None][0]
    except IndexError:
        print(word + ' --- ' + response)
    # print(split_response)
    # exit()
    for answer in split_response:  # split_response should be a list of all the definitions that came up
        # print(resp)
        # print(answer)
        if len(re.findall(r'^\[(.*?)\]', answer)) + len(re.findall(r'\](.*?)$', answer)) > 1:  # Throw away crappy
            pos_part = [re.findall(r'^\[(.*?)\]', answer)[0]]
            # print(answer)
            definition_part = re.findall(r'\](.*)', answer)[0]
            definition_part = definition_part.split(']')[-1]  # This should leave just the definition with no bracketed
            # stuff
            for mini_counter, element in enumerate(pos_part):  # Split the semicolon delimited pos entries
                if len(element.split(';')) > 1:
                    del pos_part[mini_counter]  # Delete the original, tack the new ones on to the end to avoid
                    # having to flatten the list later
                    pos_part = pos_part + element.split(';')
                if len(element.split(',')) > 1:
                    del pos_part[mini_counter]
                    pos_part = pos_part + element.split(',')
            for counter, a_pos in enumerate(pos_part):  # Get a translation for the myougi pos code from myougi
                if a_pos not in myougi_pos_responses:  # Check if it's already in the dictionary
                    pos_response_myougi = Popen(translate_myougi_codes + [a_pos], stdout=PIPE).communicate()[0].decode()
                    if '\t' in pos_response_myougi:
                        pos_response_myougi = pos_response_myougi.strip().split('\t')[1]  # Before the tab charac-
                        # ter is the query, obviously not needed
                        myougi_pos_responses[a_pos] = pos_response_myougi
                elif a_pos in myougi_pos_responses:
                    pos_response_myougi = myougi_pos_responses[a_pos]
                pos_part[counter] = pos_response_myougi
            # score = pbm_scorer(list_of_eng_sudachi_pos, pos_part)
            # if score > highest_score:
            #     highest_score = score
            #     final_definition = definition_part
    # if 'Not Found' in final_definition:
    #     try:
    #         loggy(word + response + '**error**' + str(parts_of_speech))
    #     except NameError:
    #         loggy(word + response + '**error***' + 'No Parts of Speech found.')
    # else:
    #     return [word, kana_reading, final_definition]
    # for counter, section in enumerate([f for f in answer.split('\t') if '[' in answer]):  # Splits up by row, each
    #     print(counter, section)
    #     # print(section)
    #     highest_score = 0
    #     # print(f'Section {counter} - {section}')
    #     if re.search(r'\] (.*)', section) is not None:  # Attempt to throw away useless lines
    #         pos_section = re.findall(r'^\[(.*?)\]', section)  # Attempt to capture the part of speech codes
    #         for mini_counter, element in enumerate(pos_section):  # Split the semicolon delimited pos entries
    #             if len(element.split(';')) > 1:
    #                 del pos_section[mini_counter]  # Delete the original, tack the new ones on to the end to avoid
    #                 # having to flatten the list later
    #                 pos_section = pos_section + element.split(';')
    #             if len(element.split(',')) > 1:
    #                 del pos_section[mini_counter]
    #                 pos_section = pos_section + element.split(',')
    #         for counter, a_pos in enumerate(pos_section):  # Get a translation for the myougi pos code from myougi
    #             if a_pos not in myougi_pos_responses:  # Check if it's already in the dictionary
    #                 pos_response_myougi = Popen(translate_myougi_codes + [a_pos], stdout=PIPE).communicate()[0].decode()
    #                 if '\t' in pos_response_myougi:
    #                     pos_response_myougi = pos_response_myougi.strip().split('\t')[1]  # Before the tab charac-
    #                     # ter is the query, obviously not needed
    #                     myougi_pos_responses[a_pos] = pos_response_myougi
    #             elif a_pos in myougi_pos_responses:
    #                 pos_response_myougi = myougi_pos_responses[a_pos]
    #             pos_section[counter] = pos_response_myougi
    #         score = pbm_scorer(list_of_eng_sudachi_pos, pos_section)
    #         definitions = re.findall(r'\] (.*)', section)[0]  # The definitions should rest of the thing
    #         if score > highest_score:
    #             highest_score = score
    #             best_definition = definitions
    #             # print(f'{a_pos} means \'{pos_response_myougi}\'')
    #         # print(f'{word} - {score}- {pos_section} - {list_of_eng_sudachi_pos} - {definitions}')
    #     # return best_definition


class WordAnswer:
    def __init__(self, info_list):
        self.word, self.reading, self.sudachi_reading, self.score, self.poses, self.definitions, self.pri_tags, \
            self.misc_tags, self.sudachi_poses = info_list
        self.definitions = ', '.join(self.definitions)
        self.poses = ', '.join(self.poses)
        self.pri_tags = ', '.join(self.pri_tags)
        self.misc_tags = ', '.join(self.misc_tags)
        self.sudachi_poses = ', '.join(self.sudachi_poses)
        # print('Word answer made.')

    def nice_formatting(self):
        return f'{self.word}[{self.reading}] {self.poses} - {self.definitions} | {self.pri_tags} - {self.misc_tags}'


def word_lookup(word_type_tuple):  # This is the old version that pulls from my own jmdict db rather than using
    # myougiden calls
    word, script_type, sudachi_word_pos, sudachi_reading = word_type_tuple
    word_collection = []
    highest_word_score = 0
    search_type = 'nonsense to keep there from being a result if this doesn\'t get changed'
    if script_type == 'kanji':
        search_type = 'keb'  # Picks keb or reb to get results for kana only searches as well
        priority_tag_type = 'ke_pri'
    elif script_type == 'hiragana' or 'katakana':
        search_type = 'reb'
        priority_tag_type = 're_pri'
    word_results = jmdict_conn.execute(
        f'SELECT * FROM jmdict_table WHERE raw_info LIKE "%{search_type}>{word}</{search_type}%"')
    word_lookup_results = word_results.fetchall()
    for entry_info in [f[1] for f in word_lookup_results]:
        last_pos = []  # Stores the most recent part of speech so senses without it can use the previous one
        soup = BeautifulSoup(entry_info, 'lxml')
        senses = soup.find_all('sense')
        if soup.find(priority_tag_type) is not None:
            priority_tags = [f.text for f in soup.find_all(priority_tag_type)]
        else:
            priority_tags = ['No Priority Tags']
        reading = soup.find('reb').text
        for sense in senses:
            gloss = [f.text for f in sense.find_all('gloss')]
            if sense.find('misc') is not None:  # This can hold information like 'usually kana only'
                misc = [f.text[1:-1] for f in sense.find_all('misc')]
            else:
                misc = ['No Misc Tags']
            word_pos = [trans_jm_code(f.text[1:-1]) for f in sense.find_all('pos')]
            if len(word_pos) == 0:
                word_pos = last_pos
            else:
                last_pos = word_pos
            similarity = pbm_scorer(sudachi_word_pos, word_pos, len(priority_tags))
            if reading == sudachi_reading:
                similarity += 5
            word_collection.append([word, reading, sudachi_reading, similarity, word_pos, gloss, priority_tags, misc,
                                    sudachi_word_pos])
    for entry in word_collection:
        similarity = entry[3]  # Eventually gets the highest score among all the entries
        if similarity > highest_word_score:
            highest_word_score = similarity
    for entry in word_collection:  # Is meant to return the first result with the highest score
        similarity = entry[3]
        if similarity == highest_word_score:
            return entry
    # for counter, lookup_result in enumerate(word_lookup_results):
    #     if search_type == 'reb' and script_type == 'hiragana':
    #         if ';uk;' not in lookup_result[1]:
    #             #  Deletes results for hiragana-only searches that aren't marked as uk or usually kana only
    #             del word_lookup_results[counter]
    #  The beautiful soup part is meant to *attempt* to get exact matches by only counting the first entry
    # return '\n'.join([str(f) for f in word_collection])
