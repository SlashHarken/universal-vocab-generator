#!/usr/bin/env python
# -*- coding: utf-8 -*
from setuptools import setup

setup(
    name='universal-ja-vocab-generation-dev',
    version='0.1',
    packages=[''],
    url='https://gitlab.com/SlashHarken/universal-vocab-generator',
    license='GNU General Public License v2.0',
    author='Rex',
    author_email='',
    description='Scans japanese text and creates an Anki deck with vocab words',
    install_requires=['genanki', 'sqlalchemy', 'sudachipy', 'wget', 'beautifulsoup4', 'romkan']
)
